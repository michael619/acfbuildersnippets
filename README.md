# README #

## Ajoute des snippets pour ACF Builder dans VS Code.

### Comment l'installer? ###

Utiliser le Command Palette (Ctrl+Shift+P) et chercher "Snippet".

Choisir "Preferences:  Configure User Snippets"

Choisir "PHP", copier le code du php.json présent dans ce repo et le coller dans le php.json qui vient de s'ouvrir, enregistrer et c'est bon :)
